# -- configure the spec file

configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/nagios-plugins-igwn.spec.in"
  "${CMAKE_CURRENT_SOURCE_DIR}/nagios-plugins-igwn.spec"
  @ONLY
)
