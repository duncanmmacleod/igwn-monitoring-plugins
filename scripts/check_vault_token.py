# -*- coding: utf-8 -*-
# Copyright (C) 2023 Cardiff University

"""Check that a vault token exists and is accepted by the vault.
"""

import argparse
import sys

import requests

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"
__version__ = "1.0.0"


def check_token(
    path,
    vault,
    scopes=None,
    audience=None,
    warning=0,
    critical=0,
):
    # load the token
    try:
        with open(path, "r") as file:
            token = file.read().strip()
    except Exception as exc:
        return 2, str(exc)

    try:
        resp = requests.get(
            f"https://{vault}:8200/v1/auth/token/lookup-self",
            headers={
                "X-Vault-Token": token,
            },
        )
        resp.raise_for_status()
        details = resp.json()
    except requests.HTTPError as exc:
        resp = exc.response
        return 2, f"'{resp.status_code} {resp.reason}' from {resp.request.url}"
    except requests.RequestException as exc:
        return 2, str(exc)

    remaining = details["data"]["ttl"]

    message = f"Discovered valid token with {remaining}s until expiry"
    if remaining <= critical:
        return 2, message
    if remaining <= warning:
        return 1, message
    return 0, message


def create_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version=f"%(prog)s {__version__}",
    )
    parser.add_argument(
        "-f",
        "--token-file",
        required=True,
        help=(
            "file from which to read token, if not given WLCG Beare "
            "Token Discovery protocol is used"
        ),
    )
    parser.add_argument(
        "-a",
        "--vault-host",
        required=True,
        help="hostname for vault",
    )
    parser.add_argument(
        "-w",
        "--timeleft-warning",
        default=0,
        type=float,
        help="warning threshold (seconds) on token time remaining",
    )
    parser.add_argument(
        "-c",
        "--timeleft-critical",
        default=0,
        type=float,
        help="critical threshold (seconds) on token time remaining",
    )

    return parser


def main(args=None):
    parser = create_parser()
    opts = parser.parse_args(args=args)
    status, message = check_token(
        path=opts.token_file,
        vault=opts.vault_host,
        warning=opts.timeleft_warning,
        critical=opts.timeleft_critical,
    )
    print(message)
    return status


if __name__ == "__main__":
    sys.exit(main())
